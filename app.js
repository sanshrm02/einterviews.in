var express = require('express'),
	partials = require('express-partials'),
	app = express(),
	routes = require('./routes'),
	errorHandlers = require('./middleware/errorhandlers'),
	log = require('./middleware/log'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	csrf = require('csurf'),
	session = require('express-session'),
	RedisStore = require('connect-redis')(session),
	util = require('./middleware/utilities'),
	flash = require('connect-flash'),
	config = require('./config');

app.set('view engine', 'ejs');
app.set('view options', {defaultLayout: 'layout'});

app.use(partials());
app.use(log.logger);
app.use(express.static(__dirname + '/static'));
app.use(cookieParser(config.secret));
app.use(session({
	secret: config.secret,
	saveUninitialized: false,
	resave: false,
        cookie: { maxAge: 30 * 24 * 60 * 1000 }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(csrf());
app.use(util.csrf);
app.use(util.authenticated);
app.use(flash());
app.use(util.templateRoutes);
app.use(util.templateApi);
app.use(util.defaults);
//routes
app.get('/', routes.index);
// app.get('/hairstyle-trends', routes.hairstyleTrends);
// app.get('/blog', routes.blog);
// app.get('/experience', routes.experience);
// app.get('/facial-hair-trend', routes.facialHairTrend);
// app.get('/interview', routes.interview);
// app.get('/perfect-shave', routes.perfectShave);
// app.get('/popular-shaving', routes.popularShaving);
// app.get('/add-booking', routes.addBooking);
// app.get('/services/exterior-texture-painting',routes.exteriorTexturePainting);
// app.get('/services/decorative-wall-paiting',routes.decorativeWallPainting);
// app.get('/services/kids-room-painting', routes.kidsRoomPainting);
// app.get('/services/waterproofing',routes.waterproofing);


app.use(errorHandlers.error);
app.use(errorHandlers.notFound);

app.listen(config.port);
console.log("App server running on port 3000");
