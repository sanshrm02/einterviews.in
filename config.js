var config = {
	port: 3000,
	secret: ' f887e11257eecfb47cbed89a5b2b7ad3', //md5_hash(onground.in)
	redisUrl: 'redis://localhost',
	routes: {
		login: '/login',
		logout: '/logout',
		dashboard: '/dashboard',
		gifts: '/gifts',
		events: '/events',
		inviations: '/inviations',
		shop: '/shop',
		profile: '/profile'
	},
	api:{
		key: '82bc8e47-568a-414c-819b-2cf9da75cf6b',
		serverUrl: 'http://localhost:8088'
	},
	defaults:{
		psize: 3000
	}
};

module.exports = config;
